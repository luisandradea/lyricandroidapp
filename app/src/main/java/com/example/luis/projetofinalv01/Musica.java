package com.example.luis.projetofinalv01;

/**
 * Created by luis on 18/06/18.
 */

public class Musica {
    private String id;
    private int lang;
    private String name;
    private String text;


    public Musica(String id, int lang, String name, String text) {
        this.id = id;
        this.lang = lang;
        this.name = name;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getLang() {
        return lang;
    }

    public void setLang(int lang) {
        this.lang = lang;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

package com.example.luis.projetofinalv01;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends Activity {

    EditText edtArtista;
    EditText edtMusica;
    TextView txvLetra;
    Musica musica;
    String testeJson;
    JsonObject jsonObject;


    public final String API_KEY = "5a5ca325667e6551f4a46bc354edbc94";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Recuperando elementos da view
        edtArtista = findViewById(R.id.edtArtista);
        edtMusica = findViewById(R.id.edtMusica);
        txvLetra = findViewById(R.id.txtLetra);

    }

    public void start(View view) {
        String artistaConsulta = edtArtista.getText().toString();
        String musicaConsulta = edtMusica.getText().toString();


        String url = "https://api.vagalume.com.br/search.php?art=" +artistaConsulta +"&mus=" + musicaConsulta + "&apikey=" + API_KEY ;

        new VagalumeService().execute("GET", url);
    }

    public void clean(View view) {
        edtMusica.setText("");
        edtArtista.setText("");
    }

    public void sair(View view) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Sair");

        alertDialogBuilder
                .setMessage("Deseja realmente sair?")
                .setPositiveButton("Sim",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        //finalizar aplicacao, ref: https://stackoverflow.com/questions/6330200/how-to-quit-android-application-programmatically
                        finishAndRemoveTask();
                    }
                })
             .setNegativeButton("Não",new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog,int id) {
                dialog.cancel();
            }
        });


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();


    }


    public class VagalumeService extends AsyncTask<String, Void, String>{

        private String acao = null;
        private URL url = null;
        private String method;
        private HttpURLConnection urlConnection = null;

        @Override
        protected String doInBackground(String... params) {
            Log.i("INICIANDO SERVICO", ".......................... ");

            try{

                method = params[0];
                url = new URL(params[1]);
                urlConnection = (HttpsURLConnection) url.openConnection();
                urlConnection.setReadTimeout(10000);
                urlConnection.setConnectTimeout(15000);
                urlConnection.setRequestMethod(method);
                urlConnection.connect();

                int response = urlConnection.getResponseCode();
                if(response == HttpsURLConnection.HTTP_OK){
                    Log.i("TESTE", "HTTP_OK! ");
                    Reader reader = new InputStreamReader(urlConnection.getInputStream());
                    Gson gson = new Gson();
                    jsonObject = gson.fromJson(reader,JsonObject.class);
                }
                else{
                    trataErro(response);
                }



            }
            catch (MalformedURLException e){
                e.printStackTrace();
            }
            catch (IOException e){
                e.printStackTrace();
            }
            return null;
        }

        public void trataErro(int response){
            //TODO: Expandir metodo para tratar erros requisicao http vagalume
        }


        @Override
        protected void onPostExecute(String s) {

            //processando Musica
            JsonElement musicaJson =  jsonObject.get("mus").getAsJsonArray().get(0);
            Gson gsonMusica = new Gson();
            Musica musica = gsonMusica.fromJson(musicaJson,Musica.class);

            //processando Artista
            JsonElement artistaJson = jsonObject.get("art").getAsJsonObject();
            Gson gsonArtista = new Gson();
            Artista artista = gsonArtista.fromJson(artistaJson,Artista.class);

            // mandar para outra activity
            Intent intent = new Intent(getApplicationContext(), LetraActivity.class);

            //titulo
            intent.putExtra("TITULO_TEXT", musica.getName());

            //letra
            intent.putExtra("LETRA_TEXT", musica.getText());

            //nome artista
            intent.putExtra("ARTISTA_TEXT", artista.getName());


            startActivity(intent);
        }
    }


}

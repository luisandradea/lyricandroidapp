package com.example.luis.projetofinalv01;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by luis on 30/06/18.
 */

public class LetraFragment  extends Fragment{

    TextView areaTitulo;
    TextView areaLetra;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_letra,container, false);

        areaTitulo = view.findViewById(R.id.txtTitulo);
        areaLetra = view.findViewById(R.id.txtLetra);

        return view;

    }
}

package com.example.luis.projetofinalv01;

import android.app.Fragment;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerView;

public class LetraActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    TextView areaLetra;
    TextView areaTitulo;
    Fragment fragment;

    public static final String API_KEY = "AIzaSyBfIIl5o5Gsi_J9yeQM45QajAbpa6J6FcQ";
    public static final int RECOVERY_DIALOG_REQUEST = 1;
    public static final String VIDEO_ID = "I6kfin-UeAQ";

    YouTubePlayerFragment player;

    private String titulo;
    private String autor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_letra);

        fragment = (Fragment) getFragmentManager().findFragmentById(R.id.LetraFragment);

        player = (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.ytplayer);
        player.initialize(API_KEY, this);

        //recuperando Views
        areaLetra = findViewById(R.id.txtLetra);
        areaTitulo = findViewById(R.id.txtTitulo);

        //habilitando scroll na letra
        areaLetra.setMovementMethod(new ScrollingMovementMethod());

        autor = getIntent().getStringExtra("ARTISTA_TEXT");

        //recebendo titulo da MainActivity
        titulo = getIntent().getStringExtra("TITULO_TEXT");
        areaTitulo.setText(titulo + " - " + autor);



        //recebendo letra da MainActivity
        String letra = getIntent().getStringExtra("LETRA_TEXT");
        areaLetra.setText(letra);

        //NOTIFICACAO ref: https://stackoverflow.com/questions/44489657/android-o-reporting-notification-not-posted-to-channel-but-it-is
        String NOTIFICATION_CHANNEL_ID = "4565";

        int importance = NotificationManager.IMPORTANCE_LOW;
        NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "nomeCanal", importance);
        notificationChannel.enableLights(true);
        notificationChannel.enableVibration(true);
        notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});


        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(notificationChannel);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(android.R.drawable.ic_media_play)
                .setChannelId(NOTIFICATION_CHANNEL_ID)
                .setContentText(titulo + " - " + autor)
                .setContentTitle("Cantando agora...")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        notificationManager.notify(001, builder.build());
    }

    //metodo do youtube
    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (!b) {

            youTubePlayer.cueVideo(VIDEO_ID);
        }
    }

    //metodo do youtube
    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        //TODO: metodo de falha no player do youtube;
        Log.i("Falhou!!!!", "onInitializationFailure: ");
    }

    //metodo do youtube
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(API_KEY, this);
        }
    }

    //metodo do youtube
    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView)findViewById(R.id.ytplayer);
    }

}

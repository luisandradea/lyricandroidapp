package com.example.luis.projetofinalv01;

/**
 * Created by luis on 18/06/18.
 */

public class Artista {

    private String id;
    private String name;
    private String url;

    public Artista(String id, String name, String url) {
        this.id = id;
        this.name = name;
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
